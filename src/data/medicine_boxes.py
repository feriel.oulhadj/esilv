from . import *


def read_dataset():

    fn = ROOTDIR / 'data' / 'raw' / 'OPEN_MEDIC_2019.zip'
    df = pd.read_csv(fn, sep=';', encoding='latin1', decimal=',', thousands='.')
    return df


def add_prescriptor(df):
    match_prescriptor = pd.read_csv(ROOTDIR / 'references' / 'open_medic_match_prescriptor.csv')
    dict_prescriptor = {k: v for k, v in match_prescriptor.values}
    df['l_psp_spe'] = df['PSP_SPE'].map(dict_prescriptor)
    df = df.drop(columns=['PSP_SPE'])
    return df


def add_features(df):
    df = add_prescriptor(df)
    df['name'] = df.l_cip13.apply(lambda x: x.split(' ')[0])
    return df


def pipeline():
    df = read_dataset()
    df = add_features(df)
    outfn = ROOTDIR / 'data' / 'interim' / 'OPEN_MEDIC_2019.parquet'
    df.to_parquet(outfn)
