from . import *


def read_dataset():
    fn = ROOTDIR / 'data' / 'raw' / 'breast_cancer.csv'
    df = pd.read_csv(fn, index_col='id')
    return df


def transform(df):
    df.drop(columns='Unnamed: 32', inplace=True)


def pipeline():
    df = read_dataset()
    transform(df)
    outfn = ROOTDIR / 'data' / 'interim' / 'breast_cancer.parquet'
    df.to_parquet(outfn)
