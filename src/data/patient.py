from . import *


def read_dataset():
    fn = ROOTDIR / 'data' / 'raw' / 'patient.csv'
    df = pd.read_csv(fn)
    return df


def drop_empty(df):
    null_cols = df.isnull().mean().sort_values()
    null_cols = null_cols[null_cols > 0.999].index
    df.drop(columns=null_cols, inplace=True)


def pipeline():
    df = read_dataset()
    drop_empty(df)

    outfn = ROOTDIR / 'data' / 'interim' / 'patient.parquet'
    df.to_parquet(outfn)
