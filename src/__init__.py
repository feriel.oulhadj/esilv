import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg
from sklearn import datasets
import math
from sklearn.preprocessing import StandardScaler
from pathlib import Path
import matplotlib as mpl
import missingno as msno
import click
import logging

import os

mpl.rcParams['axes.spines.top'] = False
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['text.usetex'] = False

ROOTDIR = Path(os.path.dirname(__file__)) / '..'
FIGDIR = ROOTDIR / 'figures'

if True:
    from .visualization import utils as vizutils
