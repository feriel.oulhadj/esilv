# Applied data analysis in python

This repository contains the support of the course, with notebooks and codes. All lessons are presented in the form of notebooks present in ```notebooks/report```.
The notebooks are ordered chronologically.

## Installation
In order to run the notebooks, one must intall all the requirements.

* install python and pip on your machine.
  - on [windows](https://www.youtube.com/watch?v=otmWEEFysms)
  - on [linux](https://www.youtube.com/watch?v=Yg9AkozItTU)
  - on [mac](https://www.youtube.com/watch?v=XUaJ8OctxdM)

* Install the requirered python libraries. The list of required libraries are available in the file requirements.txt. It is preferable to use a [virtual environment](https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html).

```bash
# create virtual environment
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
```
